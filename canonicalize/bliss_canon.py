import sys
from os.path import dirname

from permutation import NPNPermutation

sys.path.append(dirname(__file__)+"/PyBliss-0.50beta")
sys.path.append(dirname(__file__)+"/PyBliss-0.50beta/lib/python")
import PyBliss
from canonicalize import FunctionCertificate
from itertools import product

class BlissFunctionCertificate(FunctionCertificate):
    def npncanonical(self):
        g = PyBliss.Graph()
        varlen = self.nvars
        nnodes = 2 * (varlen + 1) + len(self.truthtable) + varlen

        # 3 vertex colors: output, inputs, rows
        groups = [{0, 1}, set(xrange(2, 2 + 2 * varlen)), set(xrange(2 + 2 * varlen, nnodes - varlen)),
                  set(xrange(nnodes-varlen, nnodes))]
        color_map = dict()
        for groupid, group in enumerate(groups, start=1):
            for v in group:
                color_map[v] = groupid

        for n in range(nnodes):
            g.add_vertex(n, color=color_map[n])


        # connect inputs and the output to the negation
        g.add_edge(0, 1)
        for var in range(2, 2 + 2 * varlen, 2):
            g.add_edge(var, var + 1)
            g.add_edge(var, nnodes-(var//2))
            g.add_edge(var+1, nnodes-(var//2))

        # connect each row of the truth table to the vertices representing input and output
        inpassign = list(product(*[(x, x + 1) for x in xrange(2, 2 + 2 * varlen, 2)]))
        for i, (ret, vars) in enumerate(zip(self.truthtable, inpassign)):
            node = i + 2 + 2 * varlen
            g.add_edge(node, ret)
            for v in vars:
                g.add_edge(node, v)

        canonical_labeling = g.canonical_labeling()
        inverse_lab = {v:k for k, v in canonical_labeling.iteritems()}

        flip_output = canonical_labeling[0] == 1

        new_first_line = inpassign[inverse_lab[2+2*varlen] - 2 - 2* varlen]
        flipsinps = [x % 2 == 1 for x in new_first_line]

        orderinps = [x//2-1 for x in sorted(new_first_line, key=lambda x :canonical_labeling[x])]
        input_map2 = dict(enumerate(zip(orderinps, flipsinps)))


        self.permutation = NPNPermutation(flip_output, input_map2)
        self.canonical_labeling = canonical_labeling
        self.certificate = g.relabel(canonical_labeling)
        pass



