from typing import Dict, Tuple, Any, Callable


class NPNPermutation(object):
    def __init__(self, flip_out, inpmap):
        # type: (bool, Dict[int, Tuple[int, bool]]) -> None
        self.flip_out = flip_out
        self.input_map = inpmap

    def apply(self, output, inputs, neg=lambda x: ~x):
        # type: (Any, Any, Callable[[Any], Any]) -> Tuple[Any, Any]
        if self.flip_out:
            retout = neg(output)
        else:
            retout = output

        retinps = inputs.copy()
        assert len(inputs) == len(self.input_map)
        for frm, (to, doflip) in self.input_map.items():
            if doflip:
                dest = neg(inputs[frm])
            else:
                dest = inputs[frm]
            retinps[to] = dest

        return retout, retinps

    def inverse(self):
        inv_input = { v: (k,f) for k, (v,f) in self.input_map.items()}
        return self.__class__(self.flip_out, inv_input)

    def __eq__(self, other):
        return self.flip_out == other.flip_out and self.input_map == other.input_map

    def __repr__(self):
        return "<NPNPerm {!r} {!r}>".format((self.flip_out), (self.input_map))