from itertools import product
import unittest
from typing import List, Union
from networkx_nauty.nauty_sparse import SparseGraph
import networkx as nx
from six.moves import xrange

class FunctionCertificate(object):
    def __init__(self, nvars, truthtable):
        # type: (int, List[int]) -> None
        """Initialize Boolean function with number of inputs and truth table as a list of outputs"""
        self.truthtable = truthtable
        self.nvars = (nvars)
        assert (len(truthtable) == (2 << (nvars - 1)))
        self.npncanonical()

    def __hash__(self):
        return hash(self.certificate)

    def __eq__(self, other):
        return self.certificate == other.certificate

    def npncanonical(self):
        """Transform the function in the NPN canonical one"""

        varlen = (self.nvars)
        nnodes = 2 * (varlen + 1) + len(self.truthtable)
        #g = nauty.Graph(nnodes)
        g = nx.Graph()
        # 3 vertex colors: output, inputs, rows
        groups = [[0,1], list(xrange(2, 2 + 2 * varlen)), list(xrange(2 + 2 * varlen, nnodes))]

        # connect inputs and the output to the negation
        g.add_edge(0, 1)
        for var in range(2, 2+2* varlen, 2):
            g.add_edge(var, var+1)

        # connect each row of the truth table to the vertices representing input and output
        inpassign = list(product(*[(x, x + 1) for x in xrange(2, 2 + 2 * varlen, 2)]))
        for i, (ret, vars) in enumerate(zip(self.truthtable, inpassign)):
            conntos = [ret]
            conntos.extend(vars)
            for connto in conntos:
                g.add_edge(i + 2 + 2 * varlen, connto)

        sg = SparseGraph(g)

        cert = sg.get_canon(groups)

        self.certificate = cert



