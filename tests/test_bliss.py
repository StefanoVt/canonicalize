from unittest import TestCase, main, skip
from canonicalize.bliss_canon import BlissFunctionCertificate as FunctionCertificate


class TestBlissFunctionCertificate(TestCase):
    def test_not(self):
        bf1 = FunctionCertificate(1,[0,1])
        bf2 = FunctionCertificate(1, [1,0])

        self.assertEqual(bf1, bf2)

    def test_and(self):
        bf1 = FunctionCertificate(2,[0,0,0, 1])
        bf2 = FunctionCertificate(2, [0, 1, 1, 1])
        self.assertEqual(bf1,bf2)

    def test_different(self):
        bf1 = FunctionCertificate(2, [0, 0, 0, 1])
        bf2 = FunctionCertificate(2, [0, 1, 0, 1])
        self.assertFalse(bf1 == bf2)

    def _test_a_perm(self, bf1):
        simple = 1, [2,3]
        negsimple =  bf1.permutation.apply(1, [2,3], neg= lambda x: -x)
        backsimple = bf1.permutation.inverse().apply(negsimple[0], negsimple[1], neg=lambda x: -x)
        self.assertEqual(bf1.permutation, bf1.permutation.inverse().inverse())
        self.assertEqual(simple, backsimple)


    def test_perm(self):
        bfs = [FunctionCertificate(1, [0, 1]),
               FunctionCertificate(1, [1, 0]),
               FunctionCertificate(2, [0, 0, 0, 1]),
               FunctionCertificate(2, [0, 1, 1, 1]),
               FunctionCertificate(2, [0, 1, 0, 1])]
        for bf in bfs:
            self._test_a_perm(bf)


if __name__ == '__main__':
    main()