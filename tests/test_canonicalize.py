from unittest import TestCase, main
from canonicalize.canonicalize import FunctionCertificate


class TestFunctionCertificate(TestCase):
    def test_not(self):
        bf1 = FunctionCertificate(1,[0,1])
        bf2 = FunctionCertificate(1, [1,0])


        self.assertEqual(bf1, bf2)

    def test_and(self):
        bf1 = FunctionCertificate(2,[0,0,0, 1])
        bf2 = FunctionCertificate(2, [0, 1, 1, 1])
        self.assertEqual(bf1,bf2)

    def test_different(self):
        bf1 = FunctionCertificate(2, [0, 0, 0, 1])
        bf2 = FunctionCertificate(2, [0, 1, 0, 1])
        self.assertFalse(bf1 == bf2)

