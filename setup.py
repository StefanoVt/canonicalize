from setuptools import setup, find_packages


setup(
    name="canonicalize",
    packages=find_packages(exclude=['tests']),
    test_suite='tests',
    install_requires=["networkx", "networkx-nauty"]
)