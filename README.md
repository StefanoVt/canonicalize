canonicalize
============

`canonicalize` is a Python library that generate a canonical object for
a class of NPN equivalent boolean functions.